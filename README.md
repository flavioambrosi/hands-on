# README #

* Código referente ao desafio proposto onde deve-se acessar a API da Marvel utilizando as seguintes tecnologias: Spring MVC 4, JAX-RS com Jersey, Jackson, ,Gson,JUnit, Tomcat

* Foi disponibilizada uma API Rest com os seguintes métodos:
   * findCharacterInfoByName(String characterName): realiza a busca de um personagem na base de dados da Marvel. Caso encontre, serão chamadas mais duas APIs da Marvel para obter alguns dados dos 20 primeiros gibis(Comic) e das 20 primeiras séries. O retorno do método é armazenado em cache.
   * findCharacterInfoByNameFromCache(String characterName): realiza a busca do personagem no cache da aplicação. Só serão retornados dados dos personagens que já foram consultados pelo método acima. O retorno será o mesmo do método findCharacterInfoByName.

* Não será realizada uma nova consulta na API da Marvel caso seja chamada a API findCharacterInfoByName com um personagem já consultado. Os dados obtidos previamente foram armazenados em cache. É possível observar esse comportamento pelo log da aplicação. 

* Subir a aplicação utilizando mvn spring-boot:run

* Exemplos de chamada da API
   http://localhost:8080/characterInfo/Spider-Man
   http://localhost:8080/characterInfoCache/Spider-Man

* Utilizado o Swagger para fazer a documentação da API, basta acessar o endereço http://localhost:8080/swagger-ui.html

* Foi criado teste unitário para a API Rest. Executar pelo comando mvn clean test