package br.com.treinamento.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import br.com.treinamento.CharacterInfo;
import br.com.treinamento.marvel.MarvelRequestDataService;

/**
 * Classe de serviço responsavel obter os dados do personagem.
 */
@Service
public class MarvelService {

	@Autowired
	MarvelRequestDataService requestDataService;

	@Autowired
	private CacheManager cacheManager;
	
	/**
	 * Busca as informações do personagem desejado na base de dados da Marvel e insere os dados no cache.
	 * 
	 * @param characterName nome do personagem desejado
	 * @return Dados do personagem, comics e series.
	 * @throws Exception caso ocorra algum erro inesperado no processamento.
	 */
	@Cacheable(value="characterInfos")
	public CharacterInfo findCharacterInfoByName(String characterName) throws Exception {
		CharacterInfo info = requestDataService.findCharacterInfo(characterName);
		return info;
	}
	
	/**
	 * Busca as informações do personagem desejado no cache da aplicação. Só será retornado dados se o metodo findCharacterInfoByName
	 * já tiver sido chamado anteriormente para o personagem desejado.
	 * 
	 * @param characterName nome do personagem desejadop
	 * @return Dados do personagem, comics e series.
	 * @throws Exception caso ocorra algum erro inesperado no processamento.
	 */
	public  CharacterInfo findCharacterInfoByNameFromCache(String characterName) throws Exception {
		Cache cache = cacheManager.getCache("characterInfos");
		
		CharacterInfo info = null;
		ValueWrapper wrapper = cache.get(characterName);
		if(wrapper != null){
			info = (CharacterInfo) wrapper.get();
		}
		return info;
	}
	
	
}
