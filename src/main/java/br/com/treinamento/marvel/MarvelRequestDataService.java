package br.com.treinamento.marvel;

import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.client.ClientConfig;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import br.com.treinamento.Character;
import br.com.treinamento.CharacterInfo;
import br.com.treinamento.Comic;
import br.com.treinamento.ResponseInfo;
import br.com.treinamento.Serie;
import br.com.treinamento.controller.MarvelInformationRest;
import br.com.treinamento.gson.CustomDeserializer;

@Component
public class MarvelRequestDataService {

	/** Chave publica para acesso a API da Marvel */
	private static final String PUBLIC_KEY = "edd9f9a77e7572edc3d2e251373f1678";

	/** Chave privada para acesso a API da Marvel */
	private static final String PRIVATE_KEY = "9919bfb510bd80e062d74acd9ab9015558580aca";

	/** URL base de acesso a API */
	private static final String BASE_URL = "http://gateway.marvel.com/v1";

	/** PATH adicionao a URL_BASE para buscar informações do Personagem */
	private static final String CHARACTER_PATH = "/public/characters?name=";
	
	private static final String COMICS_PATH = "/public/characters/characterId/comics";
	
	private static final String SERIES_PATH = "/public/characters/characterId/series";
	
	private static Log LOGGER = LogFactory.getLog(MarvelRequestDataService.class);

	/*
	 * Busca dados do personagem desejado.
	 * 
	 * @param characterName Nome do personagem desejado
	 * 
	 * @return dados do personagem desejado.
	 */
	public CharacterInfo findCharacterInfo(String characterName) throws Exception {

		LOGGER.info("Buscando dados do personagem: " + characterName);
		try {
			CharacterInfo characterInfo = new CharacterInfo();
			
			String characterUrl = BASE_URL + CHARACTER_PATH + URLEncoder.encode(characterName, "UTF-8");				
			ResponseInfo<Character> character = (ResponseInfo<Character>) sendRequest(characterUrl, Character.class);
			if(character.getData().getResults() == null || character.getData().getResults().isEmpty()){
				return null;
			}			
			characterInfo.setCharacter((Character) character.getData().getResults().get(0));
			characterInfo.setComics(this.findCharacterComics(characterInfo.getCharacter().getId()));
			characterInfo.setSeries(this.findCharacterSeries(characterInfo.getCharacter().getId()));
			
			return characterInfo;
		} catch (Exception e) {
			throw e;
		}		
	}
	
	/*
	 * Busca dados de Comics do personagem desejado.
	 * 
	 * @param characterId Id do personagem desejado
	 * 
	 * @return dados dos comics.
	 */
	private List<Comic> findCharacterComics(Long characterId) throws Exception {
		LOGGER.info("Buscando Comics do personagem: " + characterId);
		try {
			String comicPathCharacterId = BASE_URL + COMICS_PATH.replace("characterId", characterId.toString());
			
			return (List<Comic>) sendRequest(comicPathCharacterId, Comic.class).getData().getResults();
		} catch (Exception e) {
			throw e;
		}		
	}
	
	/*
	 * Busca dados de Series do personagem desejado.
	 * 
	 * @param characterId Id do personagem desejado
	 * 
	 * @return dados das series.
	 */
	private List<Serie> findCharacterSeries(Long characterId) throws Exception {
		LOGGER.info("Buscando Series do personagem: " + characterId);
		try {
			String seriesPathCharacterId = BASE_URL + SERIES_PATH.replace("characterId", characterId.toString());			
			return (List<Serie>) sendRequest(seriesPathCharacterId, Serie.class).getData().getResults();
		} catch (Exception e) {
			throw e;
		}		
	}
	
	/**
	 * Realiza a chamada na API da Marvel
	 * 
	 * @param url URL a ser chamada, conforme especificação da API
	 * @param clazz Classe do objeto a ser retornado
	 * @return dados desejados 
	 */
	private ResponseInfo<?> sendRequest(String url, Class clazz){
		LOGGER.info("Buscando dados na URL: " + url);
		
		Timestamp ts = new Timestamp(new Date().getTime());
		String hash = ts + PRIVATE_KEY + PUBLIC_KEY;
		String hashMd5 = DigestUtils.md5DigestAsHex(hash.getBytes()).toString();
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		
		WebTarget webTarget = client.target(url).queryParam("ts", ts).queryParam("apikey", PUBLIC_KEY).queryParam("hash", hashMd5);
		String result = webTarget.request(MediaType.APPLICATION_JSON).get(String.class);
		
		Type characterType = new TypeToken<ResponseInfo<?>>(){}.getType();
		Gson gson = new GsonBuilder().registerTypeAdapter(characterType, new CustomDeserializer<>(clazz)).create();
		ResponseInfo<?> characterData = gson.fromJson(result, characterType);
		
		return characterData;
	}

}
