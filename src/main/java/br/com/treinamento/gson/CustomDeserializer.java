package br.com.treinamento.gson;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import br.com.treinamento.ResponseData;
import br.com.treinamento.ResponseInfo;

/**
 * Desserializado criado para permitir que as classes ResponseData e ResponseInfo possam ser genéricas para todas as chamadas.
 */
public class CustomDeserializer<T> implements JsonDeserializer<ResponseInfo<T>> {
	private Class<T> clazz;

    public CustomDeserializer(Class<T> clazz) {
        this.clazz = clazz;
    }

	@Override
	public ResponseInfo<T> deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context)
			throws JsonParseException {
		
		ResponseInfo info = context.deserialize(jsonElement, ResponseInfo.class);
		
		JsonObject data = jsonElement.getAsJsonObject().getAsJsonObject("data");
		JsonArray arr = data.getAsJsonArray("results");
        List list = new ArrayList<>();
        for(JsonElement element : arr) {
            JsonElement innerElement = element.getAsJsonObject();
            list.add(context.deserialize(innerElement, clazz));
        }
        
        info.getData().setResults(list);
        
        return info;
	}

}
