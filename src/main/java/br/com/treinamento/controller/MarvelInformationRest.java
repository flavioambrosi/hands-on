package br.com.treinamento.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.CharacterInfo;
import br.com.treinamento.service.MarvelService;

/**
 * API Rest para busca de informações sobre personagens da Marvel.
 */
@RestController
public class MarvelInformationRest {
	
	private static Log LOGGER = LogFactory.getLog(MarvelInformationRest.class);
	
	@Autowired
	MarvelService service;

	/**
	 * Busca as informações do personagem desejado na base de dados da Marvel.
	 * 
	 * @param characterName nome do personagem desejadop
	 * @return Dados do personagem, comics e series.
	 * @throws Exception caso ocorra algum erro inesperado no processamento.
	 */
	@RequestMapping(value="/characterInfo/{characterName}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public ResponseEntity<CharacterInfo> findCharacterInfoByName(@PathVariable String characterName) throws Exception {
		LOGGER.info("Buscando informacoes diretamente na API da Marvel do personagem: " + characterName);
		
		CharacterInfo info = service.findCharacterInfoByName(characterName);	
		if(info != null){
			return new ResponseEntity<CharacterInfo>(info, HttpStatus.OK);
		} else {
			return new ResponseEntity("Personagem não encontrado na base da Marvel", HttpStatus.NOT_FOUND);
		}		
	}

	/**
	 * Busca as informações do personagem desejado no cache da aplicação. Só será retornado dados se o metodo findCharacterInfoByName
	 * já tiver sido chamado anteriormente para o personagem desejado.
	 * 
	 * @param characterName nome do personagem desejadop
	 * @return Dados do personagem, comics e series.
	 * @throws Exception caso ocorra algum erro inesperado no processamento.
	 */
	@RequestMapping(value="/characterInfoCache/{characterName}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public ResponseEntity<CharacterInfo> findCharacterInfoByNameFromCache(@PathVariable String characterName) throws Exception {
		
		LOGGER.info("Buscando informacoes no cache da aplicacao do personagem: " + characterName);
		
		CharacterInfo info = service.findCharacterInfoByNameFromCache(characterName);		
		if(info != null){
			return new ResponseEntity<CharacterInfo>(info, HttpStatus.OK);
		} else {
			return new ResponseEntity("Personagem não carregado no cache", HttpStatus.NOT_FOUND);
		}
	}
}
