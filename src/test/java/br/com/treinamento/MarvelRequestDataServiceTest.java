package br.com.treinamento;

import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.nio.charset.CharsetDecoder;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.gson.CustomDeserializer;
import br.com.treinamento.marvel.MarvelRequestDataService;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class MarvelRequestDataServiceTest {
	
	Reader fileCharacter = null;
	
	Reader fileComics = null;
	
	Reader fileSeries = null;
	
	@Autowired
	MarvelRequestDataService service;
	
	@Before
	public void init() throws UnsupportedEncodingException{
		fileCharacter = new InputStreamReader(MarvelRequestDataServiceTest.class.getClassLoader().getResourceAsStream("character.json"), "UTF-8");
		fileComics = new InputStreamReader(MarvelRequestDataServiceTest.class.getClassLoader().getResourceAsStream("comic.json"), "UTF-8");
		fileSeries = new InputStreamReader(MarvelRequestDataServiceTest.class.getClassLoader().getResourceAsStream("serie.json"), "UTF-8");
	}
	
	@Test
	public void testFindCharacterInfoNotFound() throws Exception{
		CharacterInfo resultService = service.findCharacterInfo("Spider-ManXPTO");
		
		assertNull(resultService);
	}
	
	@Test
	public void testFindCharacterInfo() throws Exception{
		CharacterInfo resultService = service.findCharacterInfo("Spider-Man");
		
		assertEquals(getResult(), resultService);
	}
	
	
	private CharacterInfo getResult(){
		Type characterType = new TypeToken<ResponseInfo<?>>() {}.getType();
		Gson gson = new GsonBuilder().registerTypeAdapter(characterType, new CustomDeserializer<>(Character.class)).create();
		ResponseInfo<Character> characterData = gson.fromJson(fileCharacter, characterType);
		
		gson = new GsonBuilder().registerTypeAdapter(characterType, new CustomDeserializer<>(Comic.class)).create();
		ResponseInfo<Comic> comic = gson.fromJson(fileComics, characterType);
		
		gson = new GsonBuilder().registerTypeAdapter(characterType, new CustomDeserializer<>(Serie.class)).create();
		ResponseInfo<Serie> serie = gson.fromJson(fileSeries, characterType);
		
		CharacterInfo result = new CharacterInfo();
		result.setCharacter((Character) characterData.getData().getResults().get(0));
		result.setComics(comic.getData().getResults());
		result.setSeries(serie.getData().getResults());
		
		return result;
	}

}
