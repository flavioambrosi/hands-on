package br.com.treinamento;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.gson.CustomDeserializer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class })
@WebAppConfiguration
public class CustomDeserializerTest {

	Reader fileJson = null;

	@Before
	public void init() throws FileNotFoundException {
		fileJson = new InputStreamReader(CustomDeserializerTest.class.getClassLoader().getResourceAsStream("fileTest.json"));
	}

	@Test
	public void testDeserializeCharacterClass() throws Exception {
		Type characterType = new TypeToken<ResponseInfo<?>>() {}.getType();
		Gson gson = new GsonBuilder().registerTypeAdapter(characterType, new CustomDeserializer<>(Character.class)).create();
		ResponseInfo<Character> characterData = gson.fromJson(fileJson, characterType);

		assertEquals(getResult(), characterData);
	}
	
	@Test
	public void testDeserializeNotCharacterClass() throws Exception {
		Type characterType = new TypeToken<ResponseInfo<?>>() {}.getType();
		Gson gson = new GsonBuilder().registerTypeAdapter(characterType, new CustomDeserializer<>(Serie.class)).create();
		ResponseInfo<Serie> characterData = gson.fromJson(fileJson, characterType);

		assertNotEquals(getResult(), characterData);
	}

	private ResponseInfo<Character> getResult() {
		ResponseInfo<Character> result = new ResponseInfo<Character>();
		result.setCode("200");
		result.setStatus("Ok");
		result.setCopyright("MARVEL");
		result.setAttributionText("2016 MARVEL");

		ResponseData<Character> data = new ResponseData<>();
		result.setData(data);

		Character character = new Character();
		character.setId(Long.valueOf(1009610));
		character.setName("Spider-Man");
		character.setDescription("Bitten by a radioactive spider.");
		data.setResults(Arrays.asList(character));
		
		return result;
	}
}
