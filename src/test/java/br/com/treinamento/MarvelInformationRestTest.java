package br.com.treinamento;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.controller.MarvelInformationRest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class})
@WebAppConfiguration
public class MarvelInformationRestTest {

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Autowired
	private MarvelInformationRest marvelInformationRest;

	@Before
	public void init() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void testFindCharacterInfoByNameFromCacheNotInCache() throws Exception {
		this.mockMvc.perform(get("/characterInfoCache/Hulk")).andExpect(status().isNotFound())
				.andExpect(content().string("Personagem não carregado no cache"));
	}

	@Test
	public void testFindCharacterInfoByNameFromCacheInCache() throws Exception {
		marvelInformationRest.findCharacterInfoByName("Spider-Man");
		this.mockMvc.perform(get("/characterInfoCache/Spider-Man")).andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
		.andExpect(jsonPath("$.character.id", is(1009610)))
		.andExpect(jsonPath("$.character.name", is("Spider-Man")))
		.andExpect(jsonPath("$.series[0].id", is(458)))
		.andExpect(jsonPath("$.series[0].title", is("Actor Presents Spider-Man and the Incredible Hulk (2003)")))
		.andExpect(jsonPath("$.series[1].id", is(9790)))
		.andExpect(jsonPath("$.series[1].title", is("Age of Heroes (2010)")))
		.andExpect(jsonPath("$.comics[0].id", is(55318)))
		.andExpect(jsonPath("$.comics[0].title", is("Amazing Spider-Man (2015) #20")))
		.andExpect(jsonPath("$.comics[1].id", is(61713)))
		.andExpect(jsonPath("$.comics[1].title", is("The Clone Conspiracy (2016) #1")));
	}

	@Test
	public void testfindCharacterInfoByNameNotInMarvelDatabase() throws Exception {
		this.mockMvc.perform(get("/characterInfo/HulkXPTO")).andExpect(status().isNotFound())
				.andExpect(content().string("Personagem não encontrado na base da Marvel"));
	}

	@Test
	public void testfindCharacterInfoByNametInMarvelDatabase() throws Exception {
		this.mockMvc.perform(get("/characterInfo/Wolverine")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.character.id", is(1009718)))
				.andExpect(jsonPath("$.character.name", is("Wolverine")))
				.andExpect(jsonPath("$.series[0].id", is(15276)))
				.andExpect(jsonPath("$.series[0].title", is("5 Ronin (2011)")))
				.andExpect(jsonPath("$.series[1].id", is(12429)))
				.andExpect(jsonPath("$.series[1].title", is("5 Ronin (2010)")))
				.andExpect(jsonPath("$.comics[0].id", is(50068)))
				.andExpect(jsonPath("$.comics[0].title", is("Wolverine (2014) #3 (Adams Variant)")))
				.andExpect(jsonPath("$.comics[1].id", is(48022)))
				.andExpect(jsonPath("$.comics[1].title", is("Amazing X-Men (2013) #4")));
	}
}